let knex = require('knex');
const app = require('./app');

knex = app.db;

knex.migrate.latest()
  .then(() => {
    app.listen(3000);
  })
  .catch((err) => {
    // TODO improve this error log
    console.log(err);
  });
