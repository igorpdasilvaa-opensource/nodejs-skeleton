const bcrypt = require('bcryptjs');

module.exports = (app) => {
  const findAll = (filter = {}) => app.db('users').where(filter).select();

  const getPasswdHash = async (passwd) => {
    const salt = bcrypt.genSaltSync(10);
    return bcrypt.hashSync(passwd, salt);
  };

  const save = async (user) => {
    const promisePasswd = getPasswdHash(user.passwd);
    const userDb = await findAll({ email: user.email });
    if (userDb && userDb.length > 0) throw new app.errors.ValidationError('A user with that email already exists');

    const newUser = { ...user };
    newUser.passwd = await promisePasswd;
    return app.db('users').insert(newUser, 'id');
  };

  return { findAll, save };
};
