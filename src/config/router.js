const express = require('express');
const swaggerUi = require('swagger-ui-express');

module.exports = (app) => {
  const unProtectedRouter = express.Router();
  unProtectedRouter.use('/auth', app.routes.auth);

  const protectedRouter = express.Router();

  app.use('/v1', unProtectedRouter);
  app.use('/v1', app.config.passport.authenticate(), protectedRouter);
  app.use('/docs', swaggerUi.serve, swaggerUi.setup(app.config.swagger));
};
