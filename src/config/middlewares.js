const bodyParser = require('body-parser');
const cors = require('cors');
const morgan = require('morgan');
const helmet = require('helmet');
const requestValidator = require('@igorpdasilvaa/validator');

module.exports = (app) => {
  app.use(helmet());
  app.use(morgan(process.env.MORGANLOGLEVEL));
  app.use(cors());
  app.use(bodyParser.json());
  app.requestValidator = requestValidator;
  app.requestValidator.loadJsonSchema('src');
  app.use(requestValidator);
};
