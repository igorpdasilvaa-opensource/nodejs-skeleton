const dotenv = require('dotenv');
const path = require('path');

const envFileContent = dotenv.config().parsed;
const envFilePathToRewrite = path.resolve(process.cwd(), `.env.${envFileContent.ENVIRONMENT}`);
dotenv.config({ path: envFilePathToRewrite });
