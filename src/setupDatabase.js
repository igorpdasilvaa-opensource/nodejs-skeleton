const { db } = require('./app');
db.migrate.latest().then(() => process.exit(0)).catch(() => process.exit(1));
