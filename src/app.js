const app = require('express')();
const consign = require('consign');
const knex = require('knex');

app.preventBreak = require('@igorpdasilvaa/controller-handler');



consign({ cwd: 'src' })
  .include('./config/dotenv.js')
  .then('./config/passport.js')
  .then('./config/swagger.js')
  .then('./config/middlewares.js')
  .then('./errors')
  .then('./services')
  .then('./controllers')
  .then('./routes')
  .then('./config/router.js')
  .into(app);
  
  app.db = knex({
    client: 'mysql',
    connection: {
      host: process.env.DB_HOST,
      user: process.env.DB_USER,
      password: process.env.DB_PASSWORD,
      database: process.env.DB_DATABASE,
      port: process.env.DB_PORT || 3306
    },
    migrations: {
      directory: 'src/migrations',
    },
  });

app.get('/', (_, res) => app.db('knex_migrations_lock').select()
  .then(() => res.status(200).send('Api running normally'))
  .catch(() => res.status(500).send('Api cannot connect to the database')));


module.exports = app;
