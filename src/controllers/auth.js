const jwt = require('jwt-simple');
const bcrypt = require('bcryptjs');

const secret = process.env.jwtsecret;

module.exports = (app) => {
  const signin = async (req, res) => {
    const [user] = await app.services.user.findAll({ email: req.body.email });
    if (user) {
      if (bcrypt.compareSync(req.body.passwd, user.passwd)) {
        const payload = {
          id: user.id,
          name: user.name,
          email: user.email,
        };
        const token = jwt.encode(payload, secret);
        return res.status(200).json({ token });
      }
    }
    throw new app.errors.ValidationError('User or password invalid');
  };

  const signup = async (req, res) => {
    const resultId = await app.services.user.save(req.body);
    [req.body.id] = resultId;
    delete req.body.passwd;
    return res.status(201).send(req.body);
  };

  return { signin, signup };
};
