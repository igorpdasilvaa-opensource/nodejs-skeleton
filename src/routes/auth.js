
const express = require('express');


module.exports = (app) => {
  const router = express.Router();
  // TODO create the swagger documentation

  router.post('/signin', app.preventBreak(app.controllers.auth.signin));

  router.post('/signup', app.preventBreak(app.controllers.auth.signup));

  return router;
};
