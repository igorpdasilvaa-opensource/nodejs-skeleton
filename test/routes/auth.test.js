const request = require('supertest');
const jwt = require('jwt-simple');
const knex = require('knex');
const app = require('../../src/app');


test('insert user successfully in signup route', () => request(app).post('/v1/auth/signup')
  .send({ name: 'inserted name', email: `${Date.now()}@gmail.com`, passwd: '123456' }).then((res) => {
    expect(res.status).toBe(201);
    expect(res.body.name).toBe('inserted name');
    expect(res.body).not.toHaveProperty('passwd');
    expect(res.body).toHaveProperty('email');
  }));

test('You must not insert a user successfully in signup, missing the field name', () => request(app).post('/v1/auth/signup')
  .send({ email: `${Date.now()}@gmail.com`, passwd: '123456' }).then((res) => {
    expect(res.status).toBe(400);
    expect(res.body.error).toBe('should have required property \'name\'');
  }));

test('You must not insert a user successfully in signup, missing the field email', () => request(app).post('/v1/auth/signup')
  .send({ name: 'inserted name', passwd: '123456' }).then((res) => {
    expect(res.status).toBe(400);
    expect(res.body.error).toBe('should have required property \'email\'');
  }));

test('You must not insert a user successfully in signup, missing the field password', () => request(app).post('/v1/auth/signup')
  .send({ name: 'inserted name', email: `${Date.now()}@gmail.com` }).then((res) => {
    expect(res.status).toBe(400);
    expect(res.body.error).toBe('should have required property \'passwd\'');
  }));

test('Must receive token when logging', async () => {
  const email = `${Date.now()}@mail.com`;

  await app.services.user.save({ name: 'token test', email, passwd: '123456' });
  const res = await request(app).post('/v1/auth/signin').send({ email, passwd: '123456' });

  expect(res.status).toBe(200);
  expect(res.body).toHaveProperty('token');
});

test('You must not authenticate user with password wrong', async () => {
  const email = `${Date.now()}@mail.com`;

  await app.services.user.save({ name: 'token test', email, passwd: '123456' });
  const res = await request(app).post('/v1/auth/signin').send({ email, passwd: '1234567' });

  expect(res.status).toBe(400);
  expect(res.body.error).toBe('User or password invalid');
});

test('You must not authenticate user with email wrong', async () => {
  const res = await request(app).post('/v1/auth/signin').send({ email: 'doesNotExist@mail.com', passwd: '1234567' });

  expect(res.status).toBe(400);
  expect(res.body.error).toBe('User or password invalid');
});

test('Must not authenticate user who did not send email', async () => {
  const res = await request(app).post('/v1/auth/signin').send({ passwd: '1234567' });

  expect(res.status).toBe(400);
  expect(res.body.error).toBe('should have required property \'email\'');
});

test('Must not authenticate user who did not send password', async () => {
  const res = await request(app).post('/v1/auth/signin').send({ email: 'doesNotExist@mail.com' });

  expect(res.status).toBe(400);
  expect(res.body.error).toBe('should have required property \'passwd\'');
});

test('must not access a protected route without a token', async () => {
  const res = await request(app).get('/v1');
  expect(res.status).toBe(401);
});

test('must not access a protected route with a wrong token', async () => {
  const payload = {
    id: -1,
    name: 'fakeUser',
    email: 'fakeMail',
  };
  const token = jwt.encode(payload, process.env.jwtsecret);
  const res = await request(app).get('/v1').set({ authorization: `bearer ${token}` });
  expect(res.status).toBe(401);
});

test('Must receive 404 when logged and hit the route /v1', async () => {
  const email = `${Date.now()}@mail.com`;

  await app.services.user.save({ name: 'token test', email, passwd: '123456' });
  let res = await request(app).post('/v1/auth/signin').send({ email, passwd: '123456' });

  res = await request(app).get('/v1').set({ authorization: `bearer ${res.body.token}` });

  expect(res.status).toBe(404);
});

test('Must receive 500 when logged but connection with database is lost', async () => {
  const email = `${Date.now()}@mail.com`;

  await app.services.user.save({ name: 'token test', email, passwd: '123456' });
  let res = await request(app).post('/v1/auth/signin').send({ email, passwd: '123456' });

  app.db = knex({
    client: 'mysql',
    connection: {
      host: 'fakeConnection',
      user: 'fakeUser',
      database: 'fakeDb',
    },
  });

  res = await request(app).get('/v1').set({ authorization: `bearer ${res.body.token}` });

  expect(res.status).toBe(500);
});
