const request = require('supertest');
const knex = require('knex');
const app = require('../src/app');

test('healthcheck must suceed returning 200 and the message: Api running normally', async () => {
  const res = await request(app).get('/');
  expect(res.status).toBe(200);
  expect(res.text).toBe('Api running normally');
});

test('healthcheck must fail returning 500 and the message: Api cannot connect to the database', async () => {
  app.db = knex({
    client: 'mysql',
    connection: {
      host: 'fakeConnection',
      user: 'fakeUser',
      database: 'fakeDb',
    },
  });

  const res = await request(app).get('/');
  expect(res.status).toBe(500);
  expect(res.text).toBe('Api cannot connect to the database');
});
